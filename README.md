# COSC 6342 Group 4 Project

This assignment tasks the group to build a reservation system for a restaurant chain that meets the following criteria:

- Two categories of users / customers: guest user or registered user.
- Users should be able to search for a table and reserve. 
  - User doesn’t need to login to the system to reserve a table. If registered users, they can login.
  - User enters name, phone, email, date and time (date picker), and # of guests for dining and system presents available tables.
  - Tables have maximum capacity limit i.e., 2, 4, 6, or 8.
  - Different combinations are allowed, and owner accommodates the seating, for example: someone requests 8 guests and table for 8 is not available but 2 + 6, or 4+4 is available. System should combine the tables and notify owner they need to combine tables. In this case System reserves both tables.
- If a guest user i.e., not a registered user, system should prompt user to register (Optional) before finalizing the reservation.
- Registered users will have these fields: 
  - Name, mailing address, billing address (checkbox if same as mailing address), Preferred Diner # (system generated), Earned points (based on $ spent i.e., $1 is 1 point), preferred payment method (cash, credit, check).
- System should track high traffic days / weekends and a hold fee is required i.e. July 4th will require valid credit card on system to reserve the table. 
  - Notify user no show will have minimum $10 charge.

## Software Architecture Diagram

![Project Software Architecture Diagram](./diagram.png)

### Frontend

Modules that will handle user registration, login, and authentication. It will also manage user profile information, including name, mailing address, billing address, and preferred payment method. The solution will leverage external APIs, such as a date and time picker API to enable users to select the desired reservation date and time.

### Backend

Modules that will handle the core functionality of the reservation system, including searching for available tables, making reservations, and tracking high-traffic days. It will also manage table capacity limits and combine tables as needed, payment processing validation, and user-earned points. This includes notifying users of their reservation details and notifying restaurant staff of table reservations and table combining. It will validate credit card information and process payments when necessary, such as for high-traffic days or no-shows.

### Database

Modules that will include handling the following SQL database tables: users table, reservations table, and tables table.

## Loading the Project

In addition to the project files using Python 3.10+, you will need to install Jinja2 and SQL Alchemy.  The installation commands are as follows:

`pip install Jinja2`

`pip install SQLAlchemy`

## Loading Project Files

To build the test value database, from the `server` folder, run the following:

```bash
python ./database.py
```

To start the webserver, from the `server` folder, run the following:

```bash
python ./server.py
```