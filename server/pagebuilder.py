import jinja2, database

logged_in_user = None
date_selected = None

def build_splash_page(date_picked: bool, date=None):
    # General page for the website
    env = jinja2.Environment(loader = jinja2.FileSystemLoader("../templates"))
    splash_template = env.get_template("splash.html")
    company_name = "TEST COMPANY"
    test_tables = None
    if date_picked:
        test_tables = database.retrieve_table_info(date)
    data = splash_template.render({
        "company_name" : company_name,
        "tables" : test_tables,
        "date_picked": date_picked
    })
    return data.encode()

def build_user_info_page():
    # User info page, once the user logs in
    env = jinja2.Environment(loader = jinja2.FileSystemLoader("../templates"))
    user_info_template = env.get_template("user_info.html")
    user = database.retrieve_user_info(logged_in_user)
    return user_info_template.render({
        "username": user.username,
        "fullname": user.fullname,
        "password": user.password,
        "phone": user.phone,
        "payment": user.payment,
        "billing": user.billing_address,
        "mailing": user.mailing_address,
        "diner_num": user.diner_num,
        "points": user.points
    }).encode()

def build_initial_reservation_page(error_status, too_high=False):
    # This page asks the user for a number of seats and a date to try and reserve on
    env = jinja2.Environment(loader = jinja2.FileSystemLoader("../templates"))
    inital_res_template = env.get_template("initial_reservation.html")
    return inital_res_template.render(
        {"error" : error_status,
        "reserve_too_high": too_high
    }).encode()

def build_table_reservation_page(num_seats, date):
    env = jinja2.Environment(loader = jinja2.FileSystemLoader("../templates"))
    table_id = database.validate_seating(num_seats, date)
    if table_id == False:
        # If no tables fitting the seating requirements are available, resubmit the page with a message
        return build_initial_reservation_page(False, True)
    else:
        # Otherwise, ask the customer to confirm their reservation
        reserve_table_template = env.get_template("reserve_table.html")
        
        return reserve_table_template.render({
            "logged_in": False if logged_in_user == None else True,
            "date" : date,
            "table_id" : table_id,
            "num_seats" : num_seats
        }).encode()

def build_user_login_page(result = None):
    # Here the user provides their username and password
    env = jinja2.Environment(loader = jinja2.FileSystemLoader("../templates"))
    login_template = env.get_template("login.html")
    return login_template.render({"result": result}).encode()

def validate_user(username, password):
    # Checks if user credentials are good with the database
    result = database.validate_user(username, password)
    if result == "VALID":
        global logged_in_user
        logged_in_user = username
        return build_user_info_page()
    else:
        return build_user_login_page(result)

def create_reservation(fullname=None, phone=None, payment=None, table_id=None, date=None):
    # User has confirmed the reservation, now it needs to be added to the database
    if logged_in_user != None:
        database.insert_reservation(username=logged_in_user, table_id=table_id, date=date)
    else:
        database.insert_reservation(fullname=fullname, phone=phone, payment=payment, table_id=table_id, date=date)
    env = jinja2.Environment(loader = jinja2.FileSystemLoader("../templates"))
    login_template = env.get_template("reservation_confirmation.html")
    return login_template.render().encode() 