import os, uuid
from sqlalchemy import create_engine, Column, Integer, String, DateTime, ForeignKey, select, insert
from sqlalchemy.orm import sessionmaker, DeclarativeBase
from datetime import datetime

class Base(DeclarativeBase):
    pass

class User(Base):
    # Represents the User table from the restaurant.db
    __tablename__ = "users"

    username = Column(String(30), primary_key=True, unique=True, nullable=False)
    password = Column(String(30), nullable=False)
    fullname = Column(String(30), nullable=False)
    mailing_address = Column(String(255))
    billing_address = Column(String(255))
    payment = Column(String(30))
    phone = Column(Integer)
    diner_num = Column(Integer, nullable=False)
    points = Column(Integer)

class Reservations(Base):
    # Represents the Reservations table from the restaurant.db
    __tablename__ = "reservations"

    reserve_id = Column(Integer, primary_key=True, nullable=False, unique=True)
    username = Column(String(30), ForeignKey("users.username"), nullable=False)
    table_id = Column(Integer, ForeignKey("tables.table_id"), nullable=False)
    date = Column(DateTime, nullable=False)

class Tables(Base):
    # Represents the Tables table from the restaurant.db
    __tablename__ = "tables"

    table_id = Column(Integer, primary_key=True, nullable=False, unique=True)
    size = Column(Integer, nullable=False)

def create_db_engine():
    # Creates the connection to the database when either reservations are added, or users/tables are checked
    if "server" not in os.getcwd():
        os.chdir(os.path.join(os.getcwd(), "server"))

    return create_engine('sqlite:///restaurant.db')

def validate_user(username, password):
    engine = create_db_engine()

    with engine.connect() as conn:
        stmt = select(User.password).where(User.username == username)
        results = conn.execute(stmt).first()
        # If results are blank, user doesn't exist
        if results == []:
            return "NO USER"
        # User provided a bad password
        if password != str(results[0]):
            return "BAD PASSWORD"
        return "VALID"

def retrieve_user_info(username):
    engine = create_db_engine()

    with engine.connect() as conn:
        # Grabs the user information
        stmt = select(User).where(User.username == username)
        results = conn.execute(stmt).first()
        return User(username=results[0],
                           password=results[1],
                           fullname=results[2],
                           mailing_address=results[3],
                           billing_address=results[4],
                           payment=results[5],
                           phone=results[6],
                           diner_num=results[7],
                           points=results[8])
    
def retrieve_table_info(date):
    engine = create_db_engine()

    year, month, day = date.split("-")
    date = datetime(year=int(year), month=int(month), day=int(day))
    with engine.connect() as conn:
        # Returns the tables that available for the date provided
        stmt = select(Tables).where(Tables.table_id.not_in(select(Reservations.table_id).where(Reservations.date == date)))
        results = conn.execute(stmt).all()
        tables = []
        for result in results:
            tables.append(Tables(table_id=result[0], size=result[1]))
        return tables
    
def validate_seating(num_seats, date):
    engine = create_db_engine()

    year, month, day = date.split("-")
    date = datetime(year=int(year), month=int(month), day=int(day))
    with engine.connect() as conn:
        stmt = select(Tables).where(Tables.table_id.not_in(select(Reservations.table_id).where(Reservations.date == date)))
        # Element[0] = Table_id, Element[1] = Size
        results = conn.execute(stmt).all()
        small_tables = []
        large_table = None
        for table in results:
            # Found a table that match number of seats
            if table[1] == num_seats:
                return table[0]
            if table[1] < num_seats:
                small_tables.append(table[1])
            else:
                if large_table is None or large_table < table[1]:
                    large_table = table[0]
        # Found a table larger than number of seats, but is the smallest that does so
        if large_table is not None:
            return large_table
        
        return False
        # # Combination of small tables will be finished in a future iteration of the system.

        # combo_list = []
        
        # small_tables.sort(reverse=True)
        # for num in range(len(small_tables), 2):
        #     possible_size =0
        #     for index in range(num):
                
def insert_reservation(table_id, date, fullname=None, phone=None, payment=None, username=None):
    engine = create_db_engine()

    year, month, day = date.split("-")
    date = datetime(year=int(year), month=int(month), day=int(day))
    with engine.connect() as conn:
        # If username is empty, then a guest needs to be added to Users first, and then the reservation can be created.
        if username == None:
            random_value = uuid.uuid4().hex()
            stmt = insert(User).values(username=random_value, password=random_value, fullname=fullname, phone=phone, payment=payment, diner_num=99999999)
            conn.execute(stmt)
            stmt = insert(Reservations).values(username=random_value, table_id=table_id, date=date)
            conn.execute(stmt)
        else:
            # Otherwise, just the reservation needs to be created
            stmt = insert(Reservations).values(username, table_id=table_id, date=date)
            conn.execute(stmt)        
        conn.commit()

def build_test_database():
    # If the database needs to be recreated with test data
    if "server" not in os.getcwd():
        os.chdir(os.path.join(os.getcwd(), "server"))

    if not os.path.exists(os.path.join(os.getcwd(), "restaurant.db")):
        engine = create_engine('sqlite:///restaurant.db')

        Base.metadata.create_all(engine)

        Session = sessionmaker(bind=engine)
        session = Session()

        # Test users

        session.add(User(username='jwilliams', password='password1', fullname='Jason Williams', mailing_address='123 Apple Street', billing_address='123 Apple Street', payment='cash', phone=9952992643, diner_num=9952992643, points=91449))
        session.add(User(username='mjackson', password='password2', fullname='Malcom Jackson', mailing_address='456 Orange Road', billing_address='456 Orange Road', payment='credit', phone=6574860255, diner_num=6574860255, points=21829))
        session.add(User(username='cmiller', password='password3', fullname='Chris Miller', mailing_address='789 Peach Avenue', billing_address='789 Peach Avenue', payment='credit', phone=1828313410, diner_num=1828313410, points=70592))
        session.add(User(username='wjohnson', password='password4', fullname='Wade Johnson', mailing_address='1234 Tree Court', billing_address='1234 Tree Court', payment='check', phone=9945791494, diner_num=9945791494, points=35464))
        session.add(User(username='nchandler', password='password5', fullname='Nancy Chandler', mailing_address='5678 First Street', billing_address='5678 First Street', payment='cash', phone=4292655097, diner_num=4292655097, points=64084))
        session.add(User(username='mcraig', password='password6', fullname='Mindy Craig', mailing_address='1200 Second Street', billing_address='1200 Second Street', payment='cash', phone=1264549890, diner_num=1264549890, points=8142))
        session.add(User(username='kjacobs', password='password7', fullname='Katie Jacobs', mailing_address='5400 Third Avenue', billing_address='5400 Third Avenue', payment='credit', phone=4292655095, diner_num=4292655095, points=24018))
        session.add(User(username='slayton', password='password8', fullname='Sarah Layton', mailing_address='7800 Fourth Avenue', billing_address='7800 Fourth Avenue', payment='check', phone=8269545884, diner_num=8269545884, points=75273))
        session.add(User(username='hdenver', password='password9', fullname='Henry Denver', mailing_address='980 Circle Drive', billing_address='980 Circle Drive', payment='cash', phone=3871179361, diner_num=3871179361, points=52358))
        session.add(User(username='dsmith', password='password10', fullname='Derrick Smith', mailing_address='650 Kings Court', billing_address='650 Kings Court', payment='credit', phone=5486554792, diner_num=5486554792, points=92747))

        # Test Reservations

        session.add(Reservations(reserve_id=60689, username='jwilliams', table_id=4, date=datetime(year=2024, month=2, day=12)))
        session.add(Reservations(reserve_id=49616, username='mjackson', table_id=3, date=datetime(year=2024, month=2, day=12)))
        session.add(Reservations(reserve_id=76952, username='cmiller', table_id=2, date=datetime(year=2024, month=6, day=7)))
        session.add(Reservations(reserve_id=71013, username='wjohnson', table_id=5, date=datetime(year=2024, month=7, day=12)))
        session.add(Reservations(reserve_id=12363, username='nchandler', table_id=4, date=datetime(year=2024, month=4, day=15)))
        session.add(Reservations(reserve_id=36946, username='mcraig', table_id=4, date=datetime(year=2024, month=3, day=10)))
        session.add(Reservations(reserve_id=95737, username='kjacobs', table_id=4, date=datetime(year=2024, month=3, day=28)))
        session.add(Reservations(reserve_id=65769, username='slayton', table_id=3, date=datetime(year=2024, month=7, day=16)))
        session.add(Reservations(reserve_id=26667, username='hdenver', table_id=4, date=datetime(year=2024, month=2, day=28)))
        session.add(Reservations(reserve_id=39410, username='dsmith', table_id=1, date=datetime(year=2024, month=1, day=19)))
        session.add(Reservations(reserve_id=27769, username='jwilliams', table_id=3, date=datetime(year=2024, month=3, day=19)))
        session.add(Reservations(reserve_id=37547, username='mjackson', table_id=6, date=datetime(year=2024, month=5, day=22)))
        session.add(Reservations(reserve_id=78528, username='cmiller', table_id=4, date=datetime(year=2024, month=8, day=20)))
        session.add(Reservations(reserve_id=74109, username='wjohnson', table_id=3, date=datetime(year=2024, month=2, day=5)))
        session.add(Reservations(reserve_id=67489, username='nchandler', table_id=2, date=datetime(year=2024, month=3, day=6)))

        # Test Tables

        session.add(Tables(table_id=1, size=2))
        session.add(Tables(table_id=2, size=4))
        session.add(Tables(table_id=3, size=6))
        session.add(Tables(table_id=4, size=8))
        session.add(Tables(table_id=5, size=10))
        session.add(Tables(table_id=6, size=12)) 
        session.add(Tables(table_id=7, size=14))

        session.commit()
        session.close()

if __name__ == "__main__":
    build_test_database()