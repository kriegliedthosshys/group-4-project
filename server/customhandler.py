import http.server, pagebuilder
import os

class CustomTCPHandler(http.server.BaseHTTPRequestHandler):

    def do_GET(self):
        data = bytearray()

        # This is to adjust between testing in VSCode vs running the code from the cmdline
        if "server" not in os.getcwd():
            os.chdir(os.path.join(os.getcwd(), "server"))

        # Generates the inital splash page for the application
        if self.requestline == "GET / HTTP/1.1" or 'splash' in self.requestline:
            data = pagebuilder.build_splash_page(False)
        # Allows for logins from users, at the moment only user can be logged in at a time
        elif "login" in self.requestline:
            data = pagebuilder.build_user_login_page()
        # Page to start the reservation process
        elif "reservation" in self.requestline:
            data = pagebuilder.build_initial_reservation_page(False)
        self.send_packet_data(data)

    def do_POST(self):
        data = bytearray()
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length).decode()
        # These action are tied to the user making a reservation
        if "reserve" in self.requestline:
            fields = post_data.split("&")
            seats, date = fields[0].split("=")[1], fields[1].split("=")[1]
            # Checks if the user provides a non-integer value
            try:
                seats = int(seats)
            except ValueError:
                # Page is sent back, with an error message presented
                data = pagebuilder.build_initial_reservation_page(True)
            #Otherwise, the reservation process continues
            data = pagebuilder.build_table_reservation_page(seats, date)
        elif "date" in self.requestline:
            # This splash displays the list of available tables for the date selected
            data = pagebuilder.build_splash_page(True, (post_data.split("=")[1]))
        elif "login" in self.requestline:
            fields = post_data.split("&")
            username, password = fields[0].split("=")[1], fields[1].split("=")[1]
            # This handles logging in the user
            data = pagebuilder.validate_user(username, password)
        # If the user confirms the reservation, the reservation needs to be placed into the database
        elif "confirm" in self.requestline:
            fields = post_data.split("&")
            for field in fields:
                name, value = field.split("=")
                if name == "name_field":
                    fullname = value
                if name == "phone_num":
                    phone = value
                if name == "payment":
                    payment = value
                if name == "table":
                    table_id = value
                if name == "data":
                    date = value
                pagebuilder.create_reservation(fullname=fullname, phone=phone, payment=payment, table_id=table_id, date=date)
        self.send_packet_data(data)

    def send_packet_data(self, data):
        self.protocol_version = "HTTP/1.1"
        self.send_response(200)
        self.send_header("Content-Type", "text/html")
        self.send_header('Content-Length', str(len(data)))
        self.end_headers()
        self.wfile.write(data)