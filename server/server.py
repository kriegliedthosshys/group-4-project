import socketserver, socket
from customhandler import CustomTCPHandler

if __name__ == '__main__':
    # Webserver opens on port 8080 locally
    HOST = socket.gethostbyname(socket.gethostname())
    PORT = 8080

    with socketserver.TCPServer((HOST, PORT), CustomTCPHandler) as httpd:
        print(f"Serving on http://{HOST}:{PORT}")
        httpd.serve_forever()